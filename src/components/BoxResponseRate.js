import React, { Component } from 'react';
import styled, { css } from 'styled-components';


// Container item
const Item = styled.section`
width:22%;
height:200px;
display: flex;
flex-direction: column; 
justify-content: center;
align-items: center;
border-radius:5px;
background-color: #21445A;
margin:10px;
`;

const RatePick = Item.extend`
flex-direction: column;
text-align: center;
justify-content: flext-start;
`;

const RatePick_title = RatePick.extend`
width:100%;
height:10%;
`;

const RatePick_content = RatePick.extend`
width:100%;
height:10%;
font-weight:bold;
font-size:45px;
margin:45px 0px 45px 0;
`;

const RatePick_footer = RatePick.extend`
width:100%;
height:10%;
`;

class BoxResponseRate extends Component {
  render() {
    const {response, streams} = this.props;    
    return (
          
          <RatePick>
          <RatePick_title>
            RESPONSE RATE
          </RatePick_title>

          <RatePick_content>
          {response}%
          </RatePick_content>

          <RatePick_footer>
          {streams} streams
          </RatePick_footer>
        </RatePick>
            
    );    
  }
}

export default BoxResponseRate;
