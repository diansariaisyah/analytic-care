const React = require('react');
const ReactDOM = require('react-dom');

const ReactHighcharts = require('react-highcharts'); // Expects that Highcharts was loaded in the code.

const config = {
  /* HighchartsConfig */
  title: {
    text: 'Category',
    margin: 50
  },
  credits: {
    enabled: false
  },
  xAxis: {
    categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
},

    series: [{
        type: 'pie',
        allowPointSelect: true,
        keys: ['name', 'y', 'selected', 'sliced'],
        data: [
            ['Apples', 29.9, false],
            ['Pears', 71.5, false],
            ['Oranges', 106.4, false],
            ['Plums', 129.2, false],
            ['Bananas', 144.0, false],
            ['Peaches', 176.0, false],
            ['Prunes', 135.6, true, true],
            ['Avocados', 148.5, false]
        ],
        showInLegend: true
    }]
};


class PieChart extends React.Component {
    componentDidMount() {
      let chart = this.refs.chart.getChart();
      chart.series[0].addPoint({x: 6, y: 12});
    }
  
    render() {
      return <ReactHighcharts config={config} ref="chart"></ReactHighcharts>;
    }
  }

  ReactDOM.render(<ReactHighcharts config = {config}></ReactHighcharts>, document.getElementById('root'));
export default PieChart;