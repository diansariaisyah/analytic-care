const React = require('react');
const ReactDOM = require('react-dom');

const ReactHighcharts = require('react-highcharts'); // Expects that Highcharts was loaded in the code.

const config = {
  /* HighchartsConfig */
  title: {
    text: 'Response Time',
    align: 'left',
    margin: 50
  },
  chart: {
    height: 510
  },
  credits: {
    enabled: false
  },
  plotOptions: {
    series: {
        marker: {
            enabled: true,
            symbol: 'circle'
        },
            stacking: 'normal'
  
    }
  },
  yAxis:{
    title: {
      enabled:false
    }
  },
  xAxis: {
    categories: ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun']
  },
  legend: {
    symbolWidth: 0,
    symbolPadding: 10
  },
  series: [{
    name: 'Tokyo',
    data: [29.9, 71.5, 106.4, 129.2, 844.0, 176.0, 213.9]
  },
  {
    name: 'Jakarta',
    data: [19.9, 61.5, 406.4, 329.2, 644.0, 276.0, 613.9],
    color: '#087cbc'
  },
  {
    name: 'Bandung',
    data: [79.9, 11.5, 206.4, 829.2, 144.0, 76.0, 13.9],
    color: '#141e49'
  }]
};


class LineChart extends React.Component {
    componentDidMount() {
      let chart = this.refs.chart.getChart();
      chart.series[0].addPoint({x: 6, y: 12});
    }
  
    render() {
      return <ReactHighcharts config={config} ref="chart"></ReactHighcharts>;
    }
  }

  ReactDOM.render(<ReactHighcharts config = {config}></ReactHighcharts>, document.getElementById('root'));
export default LineChart;