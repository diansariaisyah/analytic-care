import React, { Component } from 'react';
import styled from 'styled-components';
import { DatePicker, Menu, Dropdown, Icon } from 'antd';
import 'antd/lib/date-picker/style/css';
import 'antd/dist/antd.css';
import { LocaleProvider } from 'antd';
import enUS from 'antd/lib/locale-provider/en_US';

const Container = styled.section`
display: flex;
justify-content: space-between;
width: 98.2%;
height: 50px;
border: solid 2px #EBEEF7;
border-radius:5px;
margin: 20px 10px;
`;

const ContFilter = styled.section`
display: flex;
margin: 9px 20px;
justify-content: flex-start;
`;

const ContDate = styled.section`
display: flex;
margin: 9px 20px;
justify-content: flex-end;
`;

const dateFormat = 'DD/MM/YYYY';

const menu = (
  <Menu>
    <Menu.Item key="0">
      <a href="http://www.alipay.com/">1st Menu Filter</a>
    </Menu.Item>
    <Menu.Item key="1">
      <a href="http://www.taobao.com/">2nd Menu Filter</a>
    </Menu.Item>
    <Menu.Divider />
    <Menu.Item key="3">3d Menu Filter</Menu.Item>
  </Menu>
);

export default class FilterData extends Component {
  render() {  
    const { RangePicker } = DatePicker;
    function onChange(date, dateString) {  
      // console.log(date, dateString);
      
      var startDate = dateString[0]
      var endDate = dateString[1]
    }
    return (
        <Container>

          <ContFilter>

          <Dropdown overlay={menu} trigger={['click']}>
            <a className="ant-dropdown-link" href="#">
              Show all data <Icon type="down" />
            </a>
          </Dropdown>

          </ContFilter>

          <LocaleProvider locale={enUS}>

          <ContDate>
        
          <RangePicker onChange={onChange} format={dateFormat}/>

          </ContDate>

          </LocaleProvider>

        </Container>

            
    );    
  }
}