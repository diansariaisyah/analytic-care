import React, { Component } from 'react';
import up from './up.png';
import down from './down.png';
import styled from 'styled-components';
import { Column, Table } from 'react-virtualized';
import 'react-virtualized/styles.css';
import LineChart from './LineChartResponseTime.js';

const list = [
  { day: 'Sunday',  response: '95%', coming: '95', responded: '100' },
  { day: 'Monday',  response: '95%', coming: '95', responded: '100' },
  { day: 'Tuesday',  response: '95%', coming: '95', responded: '100' },
  { day: 'Wednesday',  response: '95%', coming: '95', responded: '100' },
  { day: 'Thursday',  response: '95%', coming: '95', responded: '100' }

  // And so on...
];

//Container Respon Rate//
const ResponRate = styled.section`
display: flex;
width: 98.2%;
height: 550px;
margin:10px;
align-items:center;
border: solid 2px #EBEEF7;
border-radius:5px;
`;

// Section graphic
const RateLeft = styled.section`
width: 40%;
height: 500px;
margin: 20px;
`;

const RateRight = styled.section`
width: 55%;
height: 540px;
`;

//Right table 1 

const TableOne = styled.table`
width: 100%;
height:150px;
border: solid 2px #EBEEF7;
display: flex;
justify-content:center;
text-align:center;
margin-top:20px;
`;

const TableOneOne = styled.table`
width: 33%;
height: 130px;
margin: 10px;
border-right: solid 2px #EBEEF7;
border-left: solid 2px #EBEEF7;
display: flex;
justify-content: center;
flex-direction: column;
text-align: center;
`;

const TableOneTwo = styled.table`
width: 33%;
height: 130px;
margin: 10px;
display: flex;
justify-content: center;
flex-direction: column;
text-align: center;
`;

const TableOneTittle = styled.section`
color: #087CBC;
font-size:16px;
text-align: center;
`;

const TableOneContent = styled.section`
font-weight:bold;
color:#59585B;
font-size:45px;
text-align: center;
`;

const TableOneFooter = styled.section`
color:#59585B;
text-align: center;
`;

const ResRateTitle = styled.section`
font-weight:bold;
color:#59585B;
margin:20px 0;
`;

const Tablee = styled.section`
width:100%;
height:300px;
color: #59585B;
`;

const pathImg = styled.section`
`;

class SectionResponseTime extends Component {
  render() {
    const {fast, fastby, slow, slowby, average, vol} = this.props;    
    return (
      
      <ResponRate>
          <RateLeft>
            <LineChart/>
          </RateLeft>

          <RateRight>
            <TableOne>
              <TableOneTwo>
                <TableOneTittle>
                  FASTEST                
                </TableOneTittle>
                <TableOneContent>
                  {fast}                
                </TableOneContent>
                <TableOneFooter>
                  by {fastby} 
                </TableOneFooter>
              </TableOneTwo>

              <TableOneOne>
                <TableOneTittle>
                  SLOWEST                
                </TableOneTittle>
                <TableOneContent>
                  {slow}                
                </TableOneContent>
                <TableOneFooter>
                  by {slowby}
                </TableOneFooter>
                
              </TableOneOne>

              <TableOneTwo>
                <TableOneTittle>
                  AVERAGE                
                </TableOneTittle>
                <TableOneContent>
                  {average}                
                </TableOneContent>
                <TableOneFooter>
                <pathImg><img src={up}/></pathImg> {vol}%
                </TableOneFooter>
              </TableOneTwo>
            </TableOne>

            <ResRateTitle>
              DAILY RESPONSE RATE
            </ResRateTitle>

          <Tablee>
            <Table
              width={720}
              height={300}
              headerHeight={20}
              rowHeight={50}
              rowCount={list.length}
              rowGetter={({ index }) => list[index]}
            >
              <Column
                label='Day'
                dataKey='day'
                width={220}
              />
              <Column
                width={220}
                label='Response Rate'
                dataKey='response'
              />
              <Column
                width={220}
                label='Coming'
                dataKey='coming'
              />
              <Column
                width={220}
                label='Responded'
                dataKey='responded'
              />
            </Table>
          </Tablee>
          
          </RateRight>

          
        </ResponRate> 
            
    );    
  }
}

export default SectionResponseTime