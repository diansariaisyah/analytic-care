import React, { Component } from 'react';
import styled from 'styled-components';

// Container item
const Item = styled.section`
width:22%;
height:200px;
display: flex;
flex-direction: column; 
justify-content: center;
align-items: center;
border-radius:5px;
background-color: #21445A;
margin:10px;
`;

const RatePick = Item.extend`
flex-direction: column;
text-align: center;
justify-content: flext-start;
`;

const RatePick_title = RatePick.extend`
width:100%;
height:10%;
`;

const RatePick_content = RatePick.extend`
width:100%;
height:10%;
font-weight:bold;
font-size:45px;
margin:45px 0px 45px 0;
`;

const RatePick_footer = RatePick.extend`
width:100%;
height:10%;
`;

class BoxPeakTime extends Component {
  render() {
    const {time, day, streams} = this.props;    
    return (
            <RatePick>
                <RatePick_title>
                PEAK TIME
                </RatePick_title>

                <RatePick_content>
                {time}
                </RatePick_content>

                <RatePick_footer>
                {day} | {streams} streams
                </RatePick_footer>
            </RatePick>
    );    
  }
}

export default BoxPeakTime;
