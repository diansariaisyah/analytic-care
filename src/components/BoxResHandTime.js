import React, { Component } from 'react';
import styled, { css } from 'styled-components';
import up from './up.png';
import down from './down.png';

const Time = styled.section`
background-color: #21445A;
width: 34%;
height: 200px;
margin: 10px;
display:inline-flex;
border-radius:5px;
`;

//Response & Handling Time

const ResHand = styled.section`
flex-direction: column;
text-align: center;
border-right:solid 2px white;
margin-bottom:11px;
margin-top:13px;
`;

const ResHand2 = styled.section`
flex-direction: column;
text-align: center;
border-right:none;
`;

const ResHand_title = ResHand.extend`
width:100%;
height:10%;
margin-top:0px;
border-right:none;
`;

const ResHand2_title = ResHand2.extend`
width:100%;
height:10%;
margin-top:13px;
`;

const ResHand_content = ResHand.extend`
width:100%;
height:10%;
font-weight:bold;
font-size:45px;
border-right:none;
margin:37px 40px 80px 0;
`;

const ResHand2_content = ResHand2.extend`
width:100%;
height:10%;
font-weight:bold;
font-size:45px;
margin:35px 40px 75px 0;
`;

const ResHand_footer = ResHand.extend`
width:100%;
height:10%;
border-right:none;
`;

const ResHand2_footer = ResHand2.extend`
width:100%;
height:10%;
`;

class BoxResHandTime extends Component {
  render() {
    const {response, responseTime, handling, handlingTime} = this.props;
    return (
        <Time>
        <ResHand>
          <ResHand_title>
            RESPONSE TIME
          </ResHand_title>

          <ResHand_content>
            {response}
          </ResHand_content>

          <ResHand_footer>
          <img src={up}/> {responseTime}
          </ResHand_footer>
        </ResHand>
        <ResHand2>
          <ResHand2_title>
            HANDLING TIME
          </ResHand2_title>

          <ResHand2_content>
            {handling}
          </ResHand2_content>

          <ResHand2_footer>
          <img src={down}/> {handlingTime}
          </ResHand2_footer>
        </ResHand2>
        </Time>
            
    );    
  }
}

export default BoxResHandTime;
