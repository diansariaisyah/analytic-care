import React, { Component } from 'react';
import styled from 'styled-components';


// Container item
const Item = styled.section`
width:22%;
height:200px;
display: flex;
flex-direction: column; 
justify-content: center;
align-items: center;
border-radius:5px;
background-color: #21445A;
margin:10px ;
`;

//Item Case & Customer
const Item_case = styled.section`
width:90%;
height:50%;
text-align:center;
display:flex;
justify-content:center;
flex-direction:column;
border-bottom: solid 2px white;
`;

const Item_customer = Item_case.extend`
border-bottom: none;
`;

const NumberCaseCustomer = styled.text`
font-weight:bold;
font-size:45px;
display:flex;
flex-direction:column;
margin-top:-10px;
`;

class BoxCaseCustomer extends Component {
  
  render() {
    const {Case, customer} = this.props;    
    return (
      
          <Item>
            <Item_case>
              <p>CASE
              <br/>
              <NumberCaseCustomer>{Case}</NumberCaseCustomer></p>
            </Item_case>

            <Item_customer>
              <p>CUSTOMER
              <br/>
              <NumberCaseCustomer>{customer}</NumberCaseCustomer></p>
            </Item_customer>
          </Item> 
            
    );    
  }
}

export default BoxCaseCustomer;
