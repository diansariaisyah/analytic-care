import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import styled from 'styled-components';
import WordCloud from './components/WordCloud.js';
import PieChart from './components/PieChart.js';
import SectionResponseRate from './components/ResponseRate.js';
import SectionResponseTime from './components/ResponseTime.js';
//4 box//
import CaseCustomer from './components/BoxCaseCustomer.js';
import ResponseRate from './components/BoxResponseRate.js';
import ResHand from './components/BoxResHandTime.js';
import PeakTime from './components/BoxPeakTime.js';
//4box//
import FilterData from './components/FilterData.js';

const Container = styled.section`
display: flex;
width: 100%;
height: 250px;
background-color: #EBEEF7;
justify-content: center;
align-items: center;
color:white;
`;

// Section Wordcloud & PieChart
const WordPie = styled.section`
display: flex;
width: 98.2%;
height: 550px;
margin:40px 10px;
align-items:center;
`;

const Word = styled.section`
display: flex;
width: 50%;
height: 550px;
border: solid 2px #EBEEF7;
border-radius:5px;
margin-right: 20px;
`;

const Pie = styled.section`
display: flex;
width: 50%;
height: 550px;
border: solid 2px #EBEEF7;
border-radius:5px;
margin-left: 20px;
`;

class App extends Component {
  
  render() {
    return (
      <div>
        <FilterData/>

        <Container>
          <CaseCustomer Case={995} customer={885} />
          <ResponseRate response={97} streams={1000}/>
          <ResHand response='00:03:29' responseTime='2 minutes faster' handling='05:48:00' handlingTime='3 minutes slower'/>
          <PeakTime time='17.00-18.00' day='Tuesday' streams={258}/>
        </Container> 

        <SectionResponseRate high={100} highday='Tuesday' low={90} lowday='Wednesday' average={97} vol={2}/>  
        <SectionResponseTime fast='01s' fastby='Budi' slow='20m 19s' slowby='Jono' average='13m' vol={2}/>

        <WordPie>
          <Word>
            <WordCloud/>            
          </Word>

          <Pie>
            <PieChart/>
          </Pie>

        </WordPie>    

        
      </div>     
        
      
    );
    
  }
}

export default App;
